# Hexo-Theme-YBB
Simple Hexo template which is Bootstrap 4 based, has some beautiful
[features](#features).

## Features

- Vanilla Bootstrap 4
- Bootswatch for different templates
- Icons are awesome with FontAwesome
- Comes with RSS, sitemap
- Search with Algolia Search
- Show to us 2 pined blog
- Disqus

## Installation

### Install with Git
```shell
cd themes
git clone https://gitlab.com/BerkhanBerkdemir/hexo-theme-ybb.git
```

### Change your main template name
```shell
vi _config.yml
```

Added this line
```yaml
theme: hexo-theme-ybb
```

## This Is Free, Seriously
Please, don't make any change the footer. It won't hurt you.

## Contributing
Check [CONTRIBUTING](CONTRIBUTING.md)

But, basically, feel free to add new feature or report a bug.

## TO-DOs
- [ ] Release version 1.0.0
- [ ] Add RSS
- [ ] Add Sitemap
- [ ] Add Algolia Search
- [X] Add Disqus
- [ ] Special place for 2 posts (I called pinned blog).

## License
Check [LICENSE](LICENSE)
